// console.log('Hello!');

// Create an object using object literal.
let trainer = {};
console.log(trainer);

// Initialize/add the object properties and methods

// Properties
trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
};

// Methods
trainer.talk = function() {
	console.log('Pikachu, I choose you!');
};

// Check all the properties and methods were properly added
console.log(trainer);

// Access object properties and methods
console.log('Result of dot notation:');
console.log(trainer.name);

// Access object properties using square bracket notation
console.log('Result of square bracket notation:');
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log('Result of talk method:');
trainer.talk();


function Pokemon(name, level) {

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	// Will accept an object as a targer
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name)
		target.health -= this.attack;
		console.log(target.name + "'s health is now reduced to " + target.health);

		if (target.health <= 0) {
			target.faint()
		};
	};

	this.faint = function() {
		console.log(this.name + " fainted.");
	};
};

// Create/instantiate a new pokemon
let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);

// Create/instantiate a new pokemon
let geodude = new Pokemon("Geodude", 8);
console.log(geodude);

// Create/instantiate a new pokemon
let mewto = new Pokemon("Mewto", 100);
console.log(mewto);

// Invoke the tackle method and target a different object
geodude.tackle(pikachu);
console.log(pikachu);

// Invoke the tackle method and target a different object
mewto.tackle(geodude);
console.log(geodude);
